<?php

namespace app\commands;

use app\models\Entity\Enums\UserRole;
use app\models\Entity\User;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UserController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionSeed()
    {
        foreach ($this->getUsersData() as $data) {
            if (null !== User::findOne(['email' => $data['email']])) {
                continue;
            }

            $user = new User();
            $user->email = $data['email'];
            $user->name = $data['name'];
            $user->role = $data['role'];
            $user->password = Yii::$app->security->generatePasswordHash('123123');

            $user->save();
        }

        return ExitCode::OK;
    }


    private function getUsersData(): array
    {
        return [
            [
                'email' => 'worker@test.ru',
                'name' => 'Работник',
                'role' => UserRole::WORKER,
            ],
            [
                'email' => 'leader@test.ru',
                'name' => 'Руководитель',
                'role' => UserRole::LEADER,
            ],
        ];
    }
}
