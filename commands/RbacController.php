<?php

namespace app\commands;

use app\components\Rule\Task\CompleteRule;
use app\components\Rule\Task\UpdateRule;
use app\models\Entity\User;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $ruleTaskUpdate = new UpdateRule();
        $ruleTaskComplete = new CompleteRule();
        $auth->add($ruleTaskComplete);
        $auth->add($ruleTaskUpdate);

        $completeTask = $auth->createPermission('completeTask');
        $completeTask->description = 'Complete a task';
        $completeTask->ruleName = $ruleTaskComplete->name;
        $auth->add($completeTask);

        $createTask = $auth->createPermission('createTask');
        $createTask->description = 'Create a task';
        $auth->add($createTask);

        $deleteTask = $auth->createPermission('deleteTask');
        $deleteTask->description = 'Create a task';
        $auth->add($deleteTask);

        $updateTask = $auth->createPermission('updateTask');
        $updateTask->description = 'Update a task';
        $updateTask->ruleName = $ruleTaskUpdate->name;
        $auth->add($updateTask);


        $worker = $auth->createRole('worker');
        $auth->add($worker);
        $auth->addChild($worker, $updateTask);
        $auth->addChild($worker, $createTask);

        $leader = $auth->createRole('leader');
        $auth->add($leader);
        $auth->addChild($leader, $updateTask);
        $auth->addChild($leader, $createTask);
        $auth->addChild($leader, $deleteTask);
        $auth->addChild($leader, $completeTask);

        $userWorker = $this->getWorker();
        if (null !== $userWorker) {
            $auth->assign($worker, $userWorker->getId());
        }

        $userLeader = $this->getLeader();
        if (null !== $userLeader) {
            $auth->assign($leader, $userLeader->getId());
        }

        return ExitCode::OK;
    }

    private function getWorker(): ?User
    {
        return User::findOne(['email' => 'worker@test.ru']);
    }

    private function getLeader(): ?User
    {
        return User::findOne(['email' => 'leader@test.ru']);
    }
}
