<?php

use app\services\TaskService;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Entity\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить задание', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => yii\grid\SerialColumn::class],

            'id',
            'user_id',
            'title',
            'description:ntext',
            'score',
            //'completed',

            [
                'class' => yii\grid\ActionColumn::class,
                'template' => '{view} {update} {delete} {complete}',
                'buttons' => [
                    'complete' => function ($url, $model, $key) {
                        $options = [
                            'title' => 'Завершить',
                            'aria-label' => 'Завершить',
                        ];

                        $url = Url::toRoute(['task/complete', 'id' => $key]);
                        $icon = Html::tag('span', '&#x2714;');

                        return Html::a($icon, $url, $options);
                    },
                ],
                'visibleButtons' => [
                    'delete' => function ($model) {
                        return \Yii::$app->user->can('deleteTask');
                    },
                    'update' => function ($model) {
                        return \Yii::$app->user->can('updateTask', ['task' => $model]);
                    },
                    'complete' => function ($model) {
                        return \Yii::$app->user->can('completeTask', ['task' => $model]);
                    },
                ]
            ],
        ],
    ]); ?>


</div>
