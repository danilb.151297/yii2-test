<?php
/* @var $model \app\models\Form\Task\EditForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="task-complete">

    <?php $form = ActiveForm::begin([
        'id' => 'task-form',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 col-form-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'score')->textInput(['type' => 'number', 'min' => 0, 'max' => 10]) ?>

    <div class="form-group">
        <div class="col-lg-11">
            <?= Html::submitButton('Завершить', ['class' => 'btn btn-primary', 'name' => 'form-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
