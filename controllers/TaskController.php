<?php

namespace app\controllers;

use app\models\Entity\Task;
use app\models\Entity\TaskSearch;
use app\services\TaskService;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    private TaskService $taskService;

    public function __construct($id, $module, TaskService $taskService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->taskService = $taskService;
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['index', 'view', 'create', 'update', 'delete', 'complete'],
                            'roles' => ['@']
                        ]
                    ]
                ]
            ]
        );
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView(int $id)
    {
        return $this->render('view', [
            'model' => $this->taskService->findByIdOrFail($id),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate()
    {
        $model = new Task();
        $model->loadDefaultValues();

        if ($this->request->isPost) {
            $task = $this->taskService->saveTask($this->request);
            if (null !== $task) {
                \Yii::$app->session->setFlash('message', 'Задание успешно сохранено');
                return $this->redirect(['view', 'id' => $task->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate(int $id)
    {
        $model = $this->taskService->findByIdOrFail($id);

        if ($this->request->isPost && $this->taskService->updateTask($model, $this->request)) {
            \Yii::$app->session->setFlash('message', 'Задание успешно сохранено');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDelete(int $id)
    {
        $task = $this->taskService->findByIdOrFail($id);
        $this->taskService->deleteTask($task);

        return $this->redirect(['index']);
    }

    /**
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionComplete(int $id)
    {
        $task = $this->taskService->findByIdOrFail($id);

        if ($this->taskService->completeTask($task)) {
            \Yii::$app->session->setFlash('message', 'Задание успешно завершено');
            return $this->redirect(['task/index']);
        }

        return $this->render('complete', [
            'model' => $task,
        ]);
    }

}
