<?php

namespace app\components\Rule\Task;

use app\models\Entity\Enums\UserRole;
use yii\rbac\Rule;

class UpdateRule extends Rule
{
    public $name = 'ruleTaskUpdate';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool|void
     */
    public function execute($user, $item, $params)
    {
        return isset($params['task'])
            && (\Yii::$app->user->identity->role === UserRole::LEADER
                || (bool)$params['task']->completed === false);
    }
}