<?php

namespace app\services;

use app\models\Entity\Task;
use app\repositories\TaskRepository;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Request;

class TaskService
{
    private TaskRepository $repository;

    public function __construct(TaskRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @throws NotFoundHttpException
     */
    public function findByIdOrFail(int $id): Task
    {
        $task = Task::findOne($id);

        if (null === $task) {
            throw new NotFoundHttpException();
        }

        return $task;
    }

    /**
     * @throws ForbiddenHttpException
     */
    public function saveTask(Request $request): ?Task
    {
        if (!\Yii::$app->user->can('createTask')) {
            throw new ForbiddenHttpException('Нет прав');
        }

        $model = new Task();

        $model->user_id = \Yii::$app->user->identity->getId();
        if ($model->load($request->post()) && $model->save()) {
            return $model;
        }

        return null;
    }

    /**
     * @throws ForbiddenHttpException
     */
    public function updateTask(Task $task, Request $request): bool
    {
        $task->user_id = \Yii::$app->user->identity->getId();

        if (!\Yii::$app->user->can('updateTask', ['task' => $task])) {
            throw new ForbiddenHttpException('Нет прав');
        }

        return $task->load($request->post())
            && $task->save();
    }

    /**
     * @throws ForbiddenHttpException
     */
    public function completeTask(Task $task): bool
    {
        if (!\Yii::$app->user->can('completeTask', ['task' => $task])) {
            throw new ForbiddenHttpException('Нет прав');
        }

        if ($task->load(Yii::$app->request->post())) {
            $task->completed = 1;
            return $task->save();
        }

        return false;
    }

    /**
     * @throws \yii\db\StaleObjectException
     * @throws \Throwable
     * @throws ForbiddenHttpException
     */
    public function deleteTask(Task $task): void
    {
        if (!\Yii::$app->user->can('deleteTask', ['task' => $task])) {
            throw new ForbiddenHttpException('Нет прав');
        }

        $task->delete();
    }
}