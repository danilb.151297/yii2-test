<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tasks}}`.
 */
class m211102_161846_create_tasks_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tasks}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->notNull(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'score' => $this->smallInteger(2)->null(),
            'completed' => $this->boolean()->notNull()->defaultValue(false)
        ]);

        $this->addForeignKey(
            'fk-tasks-user_id',
            '{{%tasks}}',
            'user_id',
            '{{%users}}',
            'id'
        );

        $this->createIndex(
            'idx-tasks-user_id',
            '{{%tasks}}',
            'user_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-tasks-user_id',
            '{{%tasks}}'
        );

        $this->dropIndex(
            'idx-tasks-user_id',
            '{{%tasks}}'
        );

        $this->dropTable('{{%tasks}}');
    }
}
