<?php

namespace app\repositories;

use app\models\Entity\Task;
use app\models\Entity\User;
use yii\data\Pagination;


class UserRepository
{
    public function findByEmail(string $email): ?User
    {
        return User::findOne(['email' => $email]);
    }
}