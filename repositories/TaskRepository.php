<?php

namespace app\repositories;

use app\models\Entity\Task;
use app\models\Entity\User;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;


class TaskRepository
{
    /**
     * @return User[]
     */
    public function findByUserPaginated(User $user, int $page): array
    {
        $q = Task::find()->where(['user_id' => $user->id]);

        $pagination = new Pagination([
            'page' => $page,
            'totalCount' => (clone $q)->count(),
            'forcePageParam' => false,
        ]);

        return $q
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
    }
}