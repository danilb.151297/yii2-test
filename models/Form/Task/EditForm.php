<?php

namespace app\models\Form\Task;

use app\models\Entity\Enums\UserRole;
use app\models\Entity\Task;
use app\models\Entity\User;
use app\repositories\UserRepository;
use phpDocumentor\Reflection\Types\Boolean;
use Yii;
use yii\base\Model;

class EditForm extends Model
{
    public ?string $title = null;
    public ?string $description = null;
    public ?int $score = null;
    public ?bool $completed = null;
    private ?Task $task;

    public function __construct(?Task $task = null, $config = [])
    {
        parent::__construct($config);
        $this->task = $task;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['title', 'required'],
            ['description', 'string'],
        ];
    }

    public function save(): bool
    {
        if (null === $this->task) {
            $this->task = new Task();
        }

        if (null === $this->task->user_id) {
            $this->task->user_id = Yii::$app->user->identity->getId();
        }

        $this->task->title = $this->title;
        $this->task->description = $this->description;

        if (Yii::$app->user->identity->role === UserRole::LEADER) {
            $this->task->score = $this->score;
            $this->task->completed = $this->completed;
        }

        return $this->task->save();
    }
}
