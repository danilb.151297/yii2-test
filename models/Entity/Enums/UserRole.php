<?php

namespace app\models\Entity\Enums;

interface UserRole
{
    const WORKER = 0;
    const LEADER = 1;
}