<?php

namespace app\models\Entity;

use app\models\Entity\Enums\UserRole;
use app\models\Entity\Task;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TaskSearch represents the model behind the search form of `app\models\Entity\Task`.
 */
class TaskSearch extends Task
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $integerFields = ['id', 'score', 'completed'];

        if (\Yii::$app->user->identity->role === UserRole::LEADER) {
            $integerFields[] = 'user_id';
        }

        return [
            [$integerFields, 'integer'],
            [['title', 'description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        /** @var User $user */
        $user = \Yii::$app->user->identity;

        if ($user->role === UserRole::WORKER) {
            $query->where(['user_id' => $user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'score' => $this->score,
            'completed' => $this->completed,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
